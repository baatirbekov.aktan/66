<?php

$heights = [183, 152, 205, 199];

$students = ['Andrew', 'Aleksandr', 'Ivan', 'Anton'];

$max = max($heights);

$total = 0;

foreach ($heights as $key => $height) {
    if ($height == $max) {
        $total = $students[$key];
    }
}

print $total . "\n";