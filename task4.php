<?php

$array = [1, 5, 4, 3, 2, 6, 44, 22, 31];

$max = max($array);

$total = 0;

for ($i = 0; $i < count($array); $i ++) {
    if ($array[$i] == $max) {
        break;
    }
    $total ++;
}

print $total . "\n";